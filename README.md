## Usage

### Bringing up

```
docker-compose up
# open your browser at http://localhost:8080
```

### Shutting down

```
docker-compose down
```

### Resetting state (db, config, etc)

```
docker-compose kill
docker-compose rm
docker volume rm matomo_matomo-db
docker volume rm matomo_matomo-html
```


